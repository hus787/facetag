var a;
function foo(img){
    var imag=document.getElementById("snap");
    var form = new FormData();
    var a_t=readCookie("a_t");
    var p_id=readCookie("p_id");
    var xhr= new XMLHttpRequest();
    xhr.fb=true;
    xhr.onreadystatechange=function(){
        if (xhr.readyState==4 && xhr.status==200){
            if(xhr.fb==true){
                xhr.open("GET","/upped",true);
                xhr.fb=false;
                xhr.send()
                imag.src="data:image/jpeg;base64,"+img;
                imag.style.display="block";
            }
        }
    }
    xhr.open("POST", "https://graph.facebook.com/"+p_id+"/photos?access_token="+a_t,true);
    var blob = dataURItoBlob("data:image/jpeg;base64,"+img);
    form.append("source",blob);
    xhr.send(form);
}

function dataURItoBlob(dataURI) {
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
    {
        var tmp = dataURI.split(',')[1].replace(/[^A-Za-z0-9\+\/\=]/g, "");
        a=tmp;
        byteString = atob(tmp);
    }
    else
    {
    byteString = unescape(dataURI.split(',')[1]);
    }
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    var bb = new (window.MozBlobBuilder || window.WebKitBlobBuilder || window.BlobBuilder);
    bb.append(ab);
    return bb.getBlob(mimeString);
}


function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
