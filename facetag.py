#! /usr/bin/env python
#License: GPLv3
#Author: Hussain Parsaiyan
#
#
import webapp2
from module.Handler import Handler

app = webapp2.WSGIApplication([ \
                                ('/gran(.*)', 'module.FacetagHandle.FacetagHandler'),
                                ('/signup','module.SignUp.SignUp'),
                                ('/mem(.*)','module.FacetagHandle.FacetagHandler'),
                                (r'/log(.*)','module.LogHandle.LogHandler'),
                                (r'(.*)','module.FacetagHandle.FacetagHandler')],debug=True)
