#! /usr/bin/env python
#License: GPLv3
#Author: Hussain Parsaiyan
#
#



from string import *
from module.Handler import Handler
from module.LogHandle import LogHandler
from module.dbHandle import *
import logging, urlparse, json, urllib2,urllib
from google.appengine.api import memcache
from module.facebook import *

App_ID="210213712434825"
App_Secret="94beada6539fb1d18c4c86147c69bd46"
App_code_add='http://www.my-udacity.appspot.com/'
class FacetagHandler(Handler):

    @classmethod
    def valid_fb_page(cls, page_url=''):
        if page_url:
            parse=urlparse.urlsplit(page_url)
            path=parse.path
            if len(path)!=0 and parse.netloc in ['facebook.com', 'www.facebook.com']:
                path=path[1:].split('/')
                if path[0] in ['pages']:
                    return path[2] if len(path)>=3 else None
                else:
                    try:
                        request_page=urllib2.urlopen("https://graph.facebook.com/"+path[0])
                        retrieved_page=request_page.read()
                        if json.loads(retrieved_page).get('first_name'):
                            return None
                        else:
                            return json.loads(retrieved_page)['id']
                    except:
                        return None
            else: return None
        else: return None


                #https://graph.facebook.com/I-hate-MADE-IN-CHINA/103914828481


    def get(self,path_arg):

        user=LogHandler.check_credentials(cookie=self.request.cookies)

        #handle requests at /grant
        logging.error(path_arg)
        if path_arg[0]=='t' and user:
            self.process_user(user)

        elif path_arg[1:]=="upped" and user:
            self.update_counter(user)
            self.response.set_status(200)

        #logged in user landed here redirect to /grant
        elif (len(path_arg)==0 or path_arg[0]=='/') and user:
            self.redirect('/grant')

        elif path_arg=="cache":
            self.update_db()

        else:
            mem=memcache.Client()
            username=mem.get('temporary')
            code=self.request.get('code')
            cookie_pass_hash=self.cookies().get('username')

            if cookie_pass_hash:
                cookie_pass_hash=cookie_pass_hash.split('|')

                if path_arg[0]=='/' and cookie_pass_hash[0] in username:
                    if code and cookie_pass_hash[1]==username[cookie_pass_hash[0]]['hash']['h']:
                        self.process_code(code,cookie_pass_hash[0])
                    else:
                        self.redirect('/signup?error=Code+Error&'+urllib.urlencode({'page':username.get('page'),'username':username.get('username'),'email':username.get('email')}))

            else:
                self.redirect('/login?attempt=True')

    def post(self,in_out):
            self.error(404)

    def process_user(self,user_id):
        entire_user=FacetagUser.entire_user_by_id(user_id)
        self.setcookie(FacetagUser.entire_user_by_id(user_id))
        self.render("grant.html",title=entire_user['user'].username)

    def process_code(self,code,username):

        mem=memcache.Client()
        user_on_temp=mem.get('temporary')[username]
        try:
            access_token_expiry_dict=get_access_token_from_code(code,App_code_add,App_ID,App_Secret)
            logging.error(access_token_expiry_dict)
            graph=GraphAPI(access_token_expiry_dict['access_token'])
            purported_page_id=user_on_temp['page_id']
            user_id={}

            for pages in graph.get_object('me/accounts')['data']:
                logging.error((pages['id'],purported_page_id))
                if pages['id']==purported_page_id:
                    user_id['page_id']=pages['id']
                    user_id['page_access_token']=pages['access_token']
                    break

            if not user_id:
                get=mem.get('temporary')
                self.deletecookie()
                del get[username]
                mem.set('temporary',get)
                self.redirect('/signup?error=Page+not+owned by+the+ user&'+urllib.urlencode({'page':user_on_temp.get('page'),'username':username,'email':user_on_temp.get('email')}))

            else:
                #add user to database and add page to database. both have seperate data models
                #add user_id to memcache and username from temporary memcache

                userid_hash=FacetagUser.add_facetaguser(username=username,pass_hash=user_on_temp['hash'],user_access_token=access_token_expiry_dict['access_token'],code=code, **user_id)
                self.deletecookie()
                self.setcookie(FacetagUser.entire_user_by_id(userid_hash.split('|')[0]))
                self.redirect('/grant')
        except :
            get=mem.get('temporary')
            self.deletecookie()
            self.redirect('/signup?error=Error+Processing.+Please +retry+in+a+while&'+urllib.urlencode({'page':user_on_temp.get('page'),'username':username,'email':user_on_temp.get('email')}))
            del get[username]
            mem.set('temporary',get)

    def update_counter(self,user):

        mem=memcache.Client()

        toupdate=mem.get("toupdate")
        if not toupdate:
            mem.set("toupdate",{})
            toupdate={}
        entire_user=FacetagUser.entire_user_by_id(user)
        page_id=entire_user["page"].key().id()

        if not toupdate.get(page_id):
            toupdate[page_id]=1
        else:
            toupdate[page_id] +=1
        mem.set("toupdate",toupdate)

    def update_db(self):
        mem=memcache.Client()
        toupdate=mem.get("toupdate")
        if not toupdate:
            return
        else:
            import time
            for page_entity_id in toupdate:
                page=FacetagPage.get_by_id(page_entity_id)
                page.tag_counts=toupdate[page_entity_id]
                page.put()
                time.sleep(0.1)