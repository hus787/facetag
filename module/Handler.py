#! /usr/bin/env python
#License: GPLv3
#Author: Hussain Parsaiyan
#
#
import jinja2
import os
import webapp2


template_dir=os.path.join(os.path.dirname(__file__),'../page')
j_env=jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                        autoescape=True,
                        trim_blocks=True)

class Handler(webapp2.RequestHandler):
    def write(self, *a,**kw):

        self.response.out.write(*a,**kw)

    def render_str(self, template,**params):

        t=j_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        from module.LogHandle import LogHandler
        user=LogHandler.check_credentials(cookie=self.request.cookies)
        self.write(self.render_str(template,user=user,**kw))

    def get_arg(self,**kw):

        if not kw:
            return self.request.GET
        #this section I do not feel has much need, but still.
        else:#more elegance possible here
            query={}
            for foo in kw:
                query[foo]=self.request.get(foo)
            return query

    def setcookie(self, entire_user):
        self.response.set_cookie("user_id",str(entire_user['user'].key().id())+'|'+entire_user['user'].pass_hash[0:-30])
        self.response.set_cookie("a_t",entire_user['page'].access_token)
        self.response.set_cookie("p_id",entire_user['page'].page_id)

    def deletecookie(self):
        for i in self.request.cookies:
            self.response.delete_cookie(i)

    def cookies(self,**kw):

        if not kw:
            return self.request.cookies
        else:#more elegance possible here
            cookie={}
            for foo in kw:
                cookie[foo]=self.request.cookies[foo]
            return cookie