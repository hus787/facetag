#! /usr/bin/env python
#License: GPLv3
#Author: Hussain Parsaiyan
#
#
from google.appengine.ext import db
from google.appengine.api import memcache
import time
import logging

def query_decorator(key):

    def another_decorator(query):
        def wrapper(self,arg):
            if arg:
                mem=memcache.Client()
                if not mem.get(key+str(arg)):
                    result=query(self,arg)
                    mem.set(key+str(arg),result)
                    return result
                else:
                    logging.error('memcache was hit')
                    return mem.get(key+str(arg))
        return wrapper
    return another_decorator

def memcache_decorator(func):
    '''
    not used anywhere yet
    '''
    def wrapper():
        func()
    return wrapper

class FacetagUser(db.Model):

    username=db.StringProperty(required=True)
    pass_hash=db.StringProperty(required=True)
    access_token=db.StringProperty(required=True)
    date_joined=db.DateTimeProperty(auto_now_add=True)
    code=db.StringProperty(required=True)

    @classmethod
    def add_facetaguser(cls, **kw):
        facetag=FacetagUser(username=kw['username'],pass_hash=kw['pass_hash']['h']+kw['pass_hash']['salt'],access_token=kw['user_access_token'],code=kw['code'])
        facetag_id=facetag.put()
        page=FacetagPage.add_page(facetaguser=facetag_id.id(),page_id=kw['page_id'],access_token=kw['page_access_token'])
        mem=memcache.Client()
        temp=mem.get('temporary')
        del temp[kw['username']]
        mem.set('temporary',temp)
        mem.set("user_present_"+kw['username'],facetag)
        mem.set("get_user_id_"+kw['username'],facetag_id.id())
        mem.set("entire_user_by_id_"+str(facetag_id.id()),{'user':facetag,'page':page})
        return  str(facetag.key().id())+'|'+facetag.pass_hash[0:-30]

    @classmethod
    @query_decorator("user_present_")
    def user_present(cls,username):

        user_id=cls.get_user_id(username)
        if not user_id:
            return None
        user_and_page=cls.entire_user_by_id(user_id)
        return user_and_page['user']

    @classmethod
    @query_decorator('get_user_id_')
    def get_user_id(cls,username):
        user=FacetagUser.all().filter('username', username)
        return user[0].key().id() if user.count() else None

    @classmethod
    @query_decorator("entire_user_by_id_")
    def entire_user_by_id(cls, user_id):
        user=FacetagUser.get_by_id(int(user_id) if type(user_id)==str else user_id)
        page=FacetagPage.all().filter('facetaguser',int(user_id))[0]
        return {"user":user, "page":page}

    @classmethod
    def sane(cls,user_id):
        '''
        check the sanity and acceptibility of userid
        might be used for passwords as well
        '''
        if user_id :
            return True



class FacetagPage(db.Model):
    facetaguser=db.IntegerProperty(required=True)
    page_id=db.StringProperty(required=True)
    access_token=db.StringProperty(required=True)
    tag_counts=db.IntegerProperty()

    @classmethod
    def add_page(cls, facetaguser, page_id, access_token,tag_counts=0):

        page=FacetagPage(facetaguser=facetaguser, page_id=page_id,access_token=access_token, tag_counts=tag_counts, photo_ids='')
        page.put()
        return page
