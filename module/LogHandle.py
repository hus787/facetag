#! /usr/bin/env python
#License: GPLv3
#Author: Hussain Parsaiyan
#
#
from module.dbHandle import *
import hashlib
import random
import hmac
import unicodedata
from string import *
from module.Handler import Handler

class LogHandler(Handler):

    @classmethod
    def hash_it(cls,**kw):
        salt=cls.salt()
        password=kw['password']
        return {'h':hmac.new(salt,password,hashlib.sha512).hexdigest(),'salt':salt}

    @classmethod
    def salt(cls):
        repository=lowercase+uppercase+digits
        return ''.join([random.choice(repository) for x in xrange(30)])

    @classmethod
    def validate_user(cls,a,**kw):
        '''
        *a* is the value of 'user_id' in the cookie
        returns the user_id
        '''
        if a:
            try:
                q=FacetagUser.entire_user_by_id(int(a.split('|')[0]))
                q=q["user"]
                if q:
                    password=q.pass_hash[0:-30]
                    if a.split('|')[1]==password:
                        return q.key().id()
            except IndexError, NameError:
                pass

    @classmethod
    def check_credentials(cls,self='',**kw):
        """
        Returns None if credentials wrong else sets the cookie and returns True
        """

        user = kw.get('username')
        cookie=kw.get('cookie')

        if user:
            user=FacetagUser.user_present(user)#was user_present previously and now ;-)
            if  user:
                user_pass=user.pass_hash
                user_pass=unicodedata.normalize('NFKD',user_pass).encode()

                if hmac.new(user_pass[-30:],kw['password'],hashlib.sha512).hexdigest()==user_pass[0:-30]:
                    #check if deleting cookie prudent or set_cookie will suffice
                    self.response.set_cookie('user_id',str(user.key().id())+'|'+user_pass[0:-30])
                    return True
                else:
                    return False
            else:
                False

        if cookie:
            return cls.validate_user(cookie.get('user_id'))


    def get(self,in_out):

        if in_out=='out':

            self.response.delete_cookie('user_id')
            self.redirect('/login')

        elif in_out=='in':

            user=LogHandler.check_credentials(cookie=self.cookies())

            if user:
                self.redirect('/grant')
            else:
               self.render('login_form.html')
        else:
            self.error(404)

    def post(self,in_out):
        if in_out=='in':
            arg=self.get_arg()
            username=self.request.get('user_id')
            password=self.request.get('password')
            if password and username:
                if self.check_credentials(self,username=username, password=password):
                    self.redirect('/grant')
                else:
                    self.render('login_form.html',user_id=username,error='Username/Password incorrect')
            else:
                self.render('login_form.html',user_id=username, error='Filling both fields is mandatory')
        elif in_out=='out':
            self.redirect('/login')
        else:
            self.error(404)