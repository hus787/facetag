#License: GPLv3
#Author: Hussain Parsaiyan
#
#
import re, urllib
from module.Handler import Handler
from module.LogHandle import LogHandler
from module.FacetagHandle import FacetagHandler
from google.appengine.api import memcache
from module.dbHandle import *
import urllib

user_id_re=re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
user_pwd_re=re.compile(r"^.{6,20}$")
user_mail_re=re.compile(r"^[\S]+@[\S]+\.[\S]+$")
I={'username':'','password':'','email':'','verify':'','page':'', "page_id":""}
O={'usererror':'','pass_mismatch':'','pass_invalid':'', 'email_invalid':'','page_invalid':'','error':''}

success=True

App_ID="210213712434825"
App_Secret="94beada6539fb1d18c4c86147c69bd46"
App_code_add='http://www.my-udacity.appspot.com/'
App_code_add_encoded=urllib.quote('http://www.my-udacity.appspot.com/')

class SignUp(Handler):


    def validate(self,**kw):

        global success,I
        if FacetagUser.user_present(I['username']):
            success=False
            O['usererror']="Username not available or invalid"
            return

        if not user_id_re.match(I['username']):
            success=False
            O['usererror']="That's not a valid username"
            return

        if not user_pwd_re.match(I['password']):
            success=False
            O['pass_invalid']="That wasn't a valid password."
            return

        if not I['password']==I['verify']:
            success=False
            O['pass_mismatch']="Your passwords didn't match."
            return

        if not (user_mail_re.match(I['email'])):
            success=False
            O['email_invalid']='That\'s not a valid email.'
            return


        page_id=FacetagHandler.valid_fb_page(I['page'])

        if not page_id:
            success=False
            O['page_invalid']="Please provide a valid page url that you own"
        else:
            I['page_id']=page_id

    def clear(self,s=''):
        """
        To clear the input and output dicts i.e. I and O. nonverbose.
        """

        global I, O
        for k in I:
            I[k]=''
        for k in O:
            O[k]=''
        global success
        success=True


    def populate(self):

       global I,O
       """to populate I and O to used later in the module"""

       for key in I:
           I[key]= self.request.POST[key]

    def get(self):
        self.clear()
        for i in I:
            if self.request.get(i):
                I[i]=urllib.unquote(self.request.get(i))
        O['error']=self.request.get("error")
        self.render('signup.html',**dict(I,**O))

    def post(self):
        self.clear()
        self.populate()
        self.validate()
        if not success:
            #self.response.out.write(I)
            I['password']=I['verify']=''
            self.render('signup.html',**dict(I,**O))
        else:
            hashed_pass=LogHandler.hash_it(password=I['password'])
            user_id_cookie_value=I['username']+'|'+hashed_pass['h']
            mem=memcache.Client()
            mem.set('temporary',{I['username']:{'hash':hashed_pass,'page':I['page'],'email':I['email'],"page_id":I['page_id']}})
            self.response.set_cookie('username', user_id_cookie_value)
            self.redirect('http://www.facebook.com/dialog/oauth?client_id='+App_ID+'&redirect_uri='+App_code_add_encoded+'&scope=publish_stream,manage_pages')